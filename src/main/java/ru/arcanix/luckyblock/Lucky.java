package ru.arcanix.luckyblock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

public class Lucky {
    CuboidSelection selection;
    Integer updateTime;
    Integer limiteSize;
    HashMap<Integer, Block> blockList;
    int minChance;

    Lucky(CuboidSelection cuboidSelection, Integer n, Integer n2, HashMap<Integer, Block> hashMap) {
        this.selection = cuboidSelection;
        this.updateTime = n;
        this.limiteSize = n2;
        this.blockList = hashMap;
        this.minChance = blockList.values().stream().map(block->block.chance).min(Integer::compareTo).orElse(0);
        this.spawnLuckyBlock();
    }

    public void spawnLuckyBlock() {
        ArrayList<Location> arrayList = this.loadListLocation(this.selection);
        int n = arrayList.size();
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Main.jp, () -> {
            Random random = new Random();
            Location location2 = arrayList.get(random.nextInt(n));
            Block block = this.getBlock(minChance+random.nextInt(101-minChance));
            int n2 = (int)arrayList.stream().filter(location -> location != null && location.getBlock().getType() != Material.AIR).count();
            if (n2 < this.limiteSize) {
                location2.getWorld().getBlockAt(location2).setTypeIdAndData(block.id, block.subid, true);
            }
        }, 1L, 20L * updateTime);
    }

    public Block getBlock(int n) {
        Block[] arrblock = new Block[1];
        this.blockList.values().forEach(block -> {
            if (block.chance <= n) {
                arrblock[0] = block;
            }
        });
        return arrblock[0];
    }

    public ArrayList<Location> loadListLocation(CuboidSelection cuboidSelection) {
        Location location = cuboidSelection.getMinimumPoint();
        Location location2 = cuboidSelection.getMaximumPoint();
        ArrayList<Location> arrayList = new ArrayList<>();
        for (int i = location.getBlockX(); i <= location2.getBlockX(); ++i) {
            for (int j = location.getBlockY(); j <= location2.getBlockY(); ++j) {
                for (int k = location.getBlockZ(); k <= location2.getBlockZ(); ++k) {
                    arrayList.add(new Location(cuboidSelection.getWorld(), i, j, k));
                }
            }
        }
        return arrayList;
    }
}

