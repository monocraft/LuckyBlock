package ru.arcanix.luckyblock;

public class Block
{
    Integer chance;
    String idBlock;
    String commands;
    Integer id;
    Byte subid;
    
    Block(final Integer chance, final String idBlock, final String commands) {
        this.subid = 0;
        this.chance = chance;
        this.idBlock = idBlock;
        this.commands = commands;
        this.convertID();
        Main.blockList.put(this.id, this);
    }
    
    public void convertID() {
        if (this.idBlock.contains(":")) {
            final String[] strID = this.idBlock.split(":");
            this.id = Integer.parseInt(strID[0]);
            this.subid = Byte.parseByte(strID[1]);
        }
        else {
            this.id = Integer.parseInt(this.idBlock);
        }
    }
}
