package ru.arcanix.luckyblock;

import org.bukkit.event.EventHandler;
import org.bukkit.entity.Player;
import org.bukkit.Bukkit;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.Location;
import org.bukkit.event.Listener;

public class BukkitListener implements Listener {
    Location loc1;
    Location loc2;
    
    BukkitListener(final Location loc1, final Location loc2) {
        this.loc1 = loc1;
        this.loc2 = loc2;
    }
    
    @EventHandler
    public void breakBlock(final BlockBreakEvent e) {
        final Player p = e.getPlayer();
        if (e.getBlock().getLocation().toVector().isInAABB(this.loc1.toVector(), this.loc2.toVector()) && Main.blockList.containsKey(e.getBlock().getTypeId())) {
            final Block block = Main.blockList.get(e.getBlock().getTypeId());
            if (block.commands.contains(";")) {
                final String[] var8;
                final String[] cmd = var8 = block.commands.split(";");
                for (int var9 = cmd.length, var10 = 0; var10 < var9; ++var10) {
                    final String s = var8[var10];
                    Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), s.replace("%player%", p.getName()));
                }
            }
            else {
                Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), block.commands.replace("%player%", p.getName()));
            }
            e.getBlock().setTypeId(0);
        }
    }
}
