package ru.arcanix.luckyblock;

import java.util.HashMap;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main
        extends JavaPlugin implements CommandExecutor {
    public static JavaPlugin jp;
    public static Integer updateTime;
    public static Integer limitSize;
    public static HashMap<Integer, Block> blockList;
    public static boolean isLucky;

    static {
        blockList = new HashMap<>();
    }

    @Override
    public void onEnable() {
        jp = this;
        this.getCommand("luckyblockreload").setExecutor(this);
        this.saveDefaultConfig();
        Main.getWorldEdit();
        updateTime = this.getConfig().getInt("updateTime");
        limitSize = this.getConfig().getInt("limitSize");
        String[] strLoc1 = this.getConfig().getString("loc1").split(",");
        Location loc1 = new Location(Bukkit.getWorld(strLoc1[0]), Integer.parseInt(strLoc1[1]), Integer.parseInt(strLoc1[2]), Integer.parseInt(strLoc1[3]));
        String[] strLoc2 = this.getConfig().getString("loc2").split(",");
        Location loc2 = new Location(Bukkit.getWorld(strLoc2[0]), Integer.parseInt(strLoc2[1]), Integer.parseInt(strLoc2[2]), Integer.parseInt(strLoc2[3]));
        CuboidSelection selection = new CuboidSelection(loc1.getWorld(), loc1, loc2);
        this.getConfig().getConfigurationSection("Blocks").getKeys(false).forEach(s -> {
            int chance = this.getConfig().getInt("Blocks." + s + ".chance");
            String idBlock = this.getConfig().getString("Blocks." + s + ".id");
            String commands = this.getConfig().getString("Blocks." + s + ".commands");
            new Block(chance, idBlock, commands);
        });
        Main.startLuckyBlock(selection);
        PluginCommand jailCommand = this.getCommand("luckyblock");
        if (jailCommand != null) {
            jailCommand.setExecutor(new CommandHandler());
        } else {
            this.getLogger().warning("\"/luckyblock\" command not found in plugin.yml. ");
        }
    }

    public static void startLuckyBlock(CuboidSelection selection) {
        if (selection != null & updateTime != null && limitSize != null && blockList.size() > 0) {
            new Lucky(selection, updateTime, limitSize, blockList);
            isLucky = true;
            Bukkit.getServer().getPluginManager().registerEvents(new BukkitListener(selection.getMinimumPoint(), selection.getMaximumPoint()), jp);
        }
    }

    public static WorldEditPlugin getWorldEdit() {
        Plugin p = Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
        return p instanceof WorldEditPlugin ? (WorldEditPlugin)p : null;
    }

    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }
        Player p = (Player)sender;
        if (p.hasPermission("luckyblockreload.reload")) {
            if (command.getName().equalsIgnoreCase("luckyblockreload")) {
                Player player1 = (Player)sender;
                jp.reloadConfig();
                player1.sendMessage(jp.getConfig().getString("ReloadConfig.reloadconfig").replace("&", "\u00a7"));
                p.playSound(p.getLocation(), Sounds.VILLAGER_YES.bukkitSound(), 2.0f, 1.0f);
                return true;
            }
        } else {
            sender.sendMessage(jp.getConfig().getString("ReloadConfig.noperm_line1").replace("&", "\u00a7"));
            sender.sendMessage(jp.getConfig().getString("ReloadConfig.noperm_line2").replace("&", "\u00a7"));
            sender.sendMessage(jp.getConfig().getString("ReloadConfig.noperm_line3").replace("&", "\u00a7"));
            sender.sendMessage(jp.getConfig().getString("ReloadConfig.noperm_line4").replace("&", "\u00a7"));
            p.playSound(p.getLocation(), Sounds.VILLAGER_HAGGLE.bukkitSound(), 2.0f, 1.0f);
        }
        return true;
    }
}

