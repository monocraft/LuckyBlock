package ru.arcanix.luckyblock;

import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class CommandHandler implements CommandExecutor
{
    public boolean onCommand(final CommandSender commandSender, final Command command, final String commandLabel, final String[] args) {
        final Player p = (Player)commandSender;
        if (args.length == 2) {
            final String point = args[1];
            final CuboidSelection selection = (CuboidSelection)Main.getWorldEdit().getSelection(p);
            if(selection == null){
                p.sendMessage(ChatColor.RED+"Сначала нужно выделить область");
                return false;
            }
            Main.jp.getConfig().set("loc1", selection.getWorld().getName() + "," + selection.getMinimumPoint().getBlockX() + "," + selection.getMinimumPoint().getBlockY() + "," + selection.getMinimumPoint().getBlockZ());
            Main.jp.getConfig().set("loc2", selection.getWorld().getName() + "," + selection.getMaximumPoint().getBlockX() + "," + selection.getMaximumPoint().getBlockY() + "," + selection.getMaximumPoint().getBlockZ());
            Main.jp.saveConfig();
            p.sendMessage(Main.jp.getConfig().getString("Messages.create").replace("&", "§"));
            p.playSound(p.getLocation(), Sounds.VILLAGER_YES.bukkitSound(), 2.0f, 1.0f);
            if (!Main.isLucky) {
                p.sendMessage(Main.jp.getConfig().getString("Messages.action").replace("&", "§"));
                p.playSound(p.getLocation(), Sounds.VILLAGER_YES.bukkitSound(), 2.0f, 1.0f);
                Main.startLuckyBlock(selection);
            }
            else {
                p.sendMessage(Main.jp.getConfig().getString("Messages.error").replace("&", "§"));
                p.playSound(p.getLocation(), Sounds.VILLAGER_HAGGLE.bukkitSound(), 2.0f, 1.0f);
            }
            return true;
        }

            p.sendMessage(Main.jp.getConfig().getString("Messages.line1").replace("&", "§"));
            p.sendMessage(Main.jp.getConfig().getString("Messages.line2").replace("&", "§"));
            p.sendMessage(Main.jp.getConfig().getString("Messages.line3").replace("&", "§"));
            p.sendMessage(Main.jp.getConfig().getString("Messages.line4").replace("&", "§"));
            p.sendMessage(Main.jp.getConfig().getString("Messages.line5").replace("&", "§"));
            p.playSound(p.getLocation(), Sounds.VILLAGER_IDLE.bukkitSound(), 2.0f, 1.0f);
            return true;
    }
}
